﻿namespace ConnectToMySqlDB
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DataGridView = new System.Windows.Forms.DataGridView();
            this.mitarbeiterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.connecttodatabaseDataSet = new ConnectToMySqlDB.connecttodatabaseDataSet();
            this.ButtonSave = new System.Windows.Forms.Button();
            this.LabelFirstName = new System.Windows.Forms.Label();
            this.TextBoxFirstname = new System.Windows.Forms.TextBox();
            this.TextBoxSecondName = new System.Windows.Forms.TextBox();
            this.LabelSecondName = new System.Windows.Forms.Label();
            this.mitarbeiterTableAdapter = new ConnectToMySqlDB.connecttodatabaseDataSetTableAdapters.mitarbeiterTableAdapter();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mitarbeiterBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.connecttodatabaseDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridView
            // 
            this.DataGridView.AllowUserToAddRows = false;
            this.DataGridView.AllowUserToDeleteRows = false;
            this.DataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataGridView.AutoGenerateColumns = false;
            this.DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn1});
            this.DataGridView.DataSource = this.mitarbeiterBindingSource;
            this.DataGridView.Location = new System.Drawing.Point(12, 12);
            this.DataGridView.MultiSelect = false;
            this.DataGridView.Name = "DataGridView";
            this.DataGridView.ReadOnly = true;
            this.DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridView.Size = new System.Drawing.Size(603, 240);
            this.DataGridView.TabIndex = 0;
            // 
            // mitarbeiterBindingSource
            // 
            this.mitarbeiterBindingSource.DataMember = "mitarbeiter";
            this.mitarbeiterBindingSource.DataSource = this.connecttodatabaseDataSet;
            // 
            // connecttodatabaseDataSet
            // 
            this.connecttodatabaseDataSet.DataSetName = "connecttodatabaseDataSet";
            this.connecttodatabaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ButtonSave
            // 
            this.ButtonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.ButtonSave.Location = new System.Drawing.Point(451, 258);
            this.ButtonSave.Name = "ButtonSave";
            this.ButtonSave.Size = new System.Drawing.Size(164, 58);
            this.ButtonSave.TabIndex = 1;
            this.ButtonSave.Text = "Eintragen";
            this.ButtonSave.UseVisualStyleBackColor = true;
            this.ButtonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // LabelFirstName
            // 
            this.LabelFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelFirstName.AutoSize = true;
            this.LabelFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.LabelFirstName.Location = new System.Drawing.Point(13, 261);
            this.LabelFirstName.Name = "LabelFirstName";
            this.LabelFirstName.Size = new System.Drawing.Size(69, 17);
            this.LabelFirstName.TabIndex = 2;
            this.LabelFirstName.Text = "Vorname:";
            // 
            // TextBoxFirstname
            // 
            this.TextBoxFirstname.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TextBoxFirstname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.TextBoxFirstname.Location = new System.Drawing.Point(99, 260);
            this.TextBoxFirstname.Name = "TextBoxFirstname";
            this.TextBoxFirstname.Size = new System.Drawing.Size(187, 23);
            this.TextBoxFirstname.TabIndex = 3;
            // 
            // TextBoxSecondName
            // 
            this.TextBoxSecondName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TextBoxSecondName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.TextBoxSecondName.Location = new System.Drawing.Point(99, 289);
            this.TextBoxSecondName.Name = "TextBoxSecondName";
            this.TextBoxSecondName.Size = new System.Drawing.Size(187, 23);
            this.TextBoxSecondName.TabIndex = 4;
            // 
            // LabelSecondName
            // 
            this.LabelSecondName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelSecondName.AutoSize = true;
            this.LabelSecondName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.LabelSecondName.Location = new System.Drawing.Point(13, 292);
            this.LabelSecondName.Name = "LabelSecondName";
            this.LabelSecondName.Size = new System.Drawing.Size(80, 17);
            this.LabelSecondName.TabIndex = 5;
            this.LabelSecondName.Text = "Nachname:";
            // 
            // mitarbeiterTableAdapter
            // 
            this.mitarbeiterTableAdapter.ClearBeforeFill = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Vorname";
            this.dataGridViewTextBoxColumn2.HeaderText = "Vorname";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Nachname";
            this.dataGridViewTextBoxColumn1.HeaderText = "Nachname";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(627, 328);
            this.Controls.Add(this.LabelSecondName);
            this.Controls.Add(this.TextBoxSecondName);
            this.Controls.Add(this.TextBoxFirstname);
            this.Controls.Add(this.LabelFirstName);
            this.Controls.Add(this.ButtonSave);
            this.Controls.Add(this.DataGridView);
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MySql Datenbank Zugriff";
            this.Load += new System.EventHandler(this.Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mitarbeiterBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.connecttodatabaseDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridView;
        private System.Windows.Forms.Button ButtonSave;
        private System.Windows.Forms.Label LabelFirstName;
        private System.Windows.Forms.TextBox TextBoxFirstname;
        private System.Windows.Forms.TextBox TextBoxSecondName;
        private System.Windows.Forms.Label LabelSecondName;
        private connecttodatabaseDataSet connecttodatabaseDataSet;
        private System.Windows.Forms.BindingSource mitarbeiterBindingSource;
        private connecttodatabaseDataSetTableAdapters.mitarbeiterTableAdapter mitarbeiterTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
    }
}

