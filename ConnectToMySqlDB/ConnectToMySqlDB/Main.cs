﻿using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;

namespace ConnectToMySqlDB
{
    public partial class Main : Form
    {
        private readonly MySqlConnection con = new MySqlConnection(Properties.Settings.Default.MySqlDatabaseConnection);

        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            // TODO: Diese Codezeile lädt Daten in die Tabelle "connecttodatabaseDataSet.mitarbeiter". Sie können sie bei Bedarf verschieben oder entfernen.
            mitarbeiterTableAdapter.Fill(connecttodatabaseDataSet.mitarbeiter);
            FillDataGridView();
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            if (CheckInputFields())
            {
                try
                {
                    con.Open();

                    if (!CheckNameInDB(TextBoxSecondName.Text))
                    {
                        MySqlCommand createNewData = new MySqlCommand("INSERT INTO Mitarbeiter (Nachname, Vorname) VALUES('" + TextBoxSecondName.Text + "', '" + TextBoxFirstname.Text + "')", con);
                        var rowsEffected = createNewData.ExecuteNonQuery();

                        con.Close();

                        MessageBox.Show("Anzahl neuer Daten: " + rowsEffected);
                        
                        ClearTextBox();
                        FillDataGridView();
                    }
                    else
                    {
                        MessageBox.Show("Achtung der Name ist in der Datenabank schon vorhanden.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Es ist ein Fehler aufgetreten: " + ex.Message);
                }

                con.Close();
            }
            else
            {
                MessageBox.Show("Bitte füllen Sie alle Felder aus.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private bool CheckInputFields()
        {
            if (TextBoxFirstname.Text != string.Empty ||
                TextBoxSecondName.Text != string.Empty)
            {
                return true;
            }

            return false;
        }

        private bool CheckNameInDB(string name)
        {
            try
            {
                MySqlCommand checkName = new MySqlCommand("SELECT Nachname FROM Mitarbeiter WHERE Nachname = '" + name + "'", con);
                MySqlDataReader reader = checkName.ExecuteReader();

                while (reader.Read())
                {
                    string result = reader["Nachname"].ToString();

                    if (result == name)
                    {
                        reader.Close();
                        return true;
                    }
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return false;
        }

        private void FillDataGridView()
        {
            mitarbeiterTableAdapter.ClearBeforeFill = true;
            mitarbeiterTableAdapter.Fill(connecttodatabaseDataSet.mitarbeiter);

        }

        private void ClearTextBox()
        {
            TextBoxFirstname.Clear();
            TextBoxSecondName.Clear();
        }
    }
}
